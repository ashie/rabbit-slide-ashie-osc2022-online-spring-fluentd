# Fluentdの最新情報

subtitle
:  主にプロジェクト運営について

author
:  Takuro Ashie

content-source
:  OSC2022 Online/Spring

allotted-time
:  20m

theme
:  .

# 今日お話すること

* Fluentdのプロジェクト体制
* 開発方針
* 最近のリリース情報
* 最新情報の入手・開発参加方法
* Fluentdの今後

# 私

* GitHub: @ashie
* 2000年頃〜
  * 自由なソフトウェアの開発
    {::note}GNU/Linux用日本語入力開発お手伝い{:/note}
    {::note}GeckoベースのWebブラウザ...{:/note}
* 2019年〜 Fluentd開発参加

# 今日お話すること

* **Fluentdのプロジェクト体制**
* 開発方針
* 最近のリリース情報
* 最新情報の入手・開発参加方法
* Fluentdの今後
  
# Fluentdプロジェクト体制

* CNCF認定プロジェクト
  * 現在も傘下プロジェクト
    {::note}https://www.cncf.io/projects/fluentd/{:/note}
  * "`Graduated`" は成熟度を表す言葉
* 開発体制はオープン
  * GitHub上で複数企業・個人が開発
  
# 2020年までの開発体制

2020年のFluentdコミット数

* 総数: 586
  * Treasure Data: 403
  * クリアコード: 142
  * 残りは様々な企業・個人
  
# 直近の開発体制

2021年のFluentdコミット数

* 総数: 463
  * クリアコード: 301
  * Calyptia: 85
  * Treasure Data: 17
  * 残りは様々な企業・個人
	
# 端的に言うと

* 開発体制が変わりました！
  
# 開発体制の引き継ぎ

* 〜2021年2月
  * @repeatedly氏が長らく開発リード
* 2021年3月〜
  * リリース担当変更
  * @repeatedly氏は引き続きメンバー
    * 適宜アドバイス頂く

# 今日お話すること

* ~~Fluentdのプロジェクト体制~~
* **開発方針**
* 最近のリリース情報
* 最新情報の入手・開発参加方法
* Fluentdの今後

# 開発方針

昨年は以下を重要視

* メンテナンスの継続
* 属人性の解消

{::note}体制が変わった直後のため・前半のお話の動機はここから{:/note}

# メンテナンスの継続

* issue・プルリクエストに対応
  {::note}貢献者のやる気を削がない{:/note}
* CIの監視
  {::note}黙っていてもソフトウェアは壊れていく{:/note}
* 定期的にリリース
  {::note}開発リポジトリの中に引きこもらない{:/note}
* 新機能開発は控えめ

# 属人性の解消

* 複数人でリリース作業
* リリースの定式化
  * リリースサイクル
  * リリース手順

# リリースについて

Fluentdの配布形式

* gem
* Docker・k8s daemonset
* rpm/deb/msi/dmgパッケージ
  * td-agent
  * calyptia-fluentd

# Fluentd リリース方針

* 毎月29日頃
  * 肉の日リリース（2月は9日）
* gemを最初にリリース
  * Dockerイメージも追従してリリース
  * td-agentは後述

# Fluentd バージョン指針

* teenyバージョンアップ
  * バグフィックス・軽微な機能追加
* minorバージョンアップ
  * 大きな機能追加・変更
* 判断基準は曖昧

# td-agentの開発

* td-agentもバザール開発
  * fluent-package-builder
    * {::note}https://github.com/fluent/fluent-package-builder/{:/note}
      {::note}（旧名: td-agent-builder）{:/note}
  * {::note}Calyptiaからもプルリク{:/note}
    * {::note}https://github.com/fluent/fluent-package-builder/pull/358{:/note}

# td-agent リリース方針

* td-agent
  * 3〜4ヶ月毎にリリース
  * その時点の最新のFluentdや各種gemをベースに調整
  * Rubyのteenyリリースに追従

# td-agent バージョン指針

* minorバージョンアップ
  * Fluentdのminorアップに追従
* majorバージョンアップ
  * Fluentdのmajorアップ
  * Rubyのminor or majorアップ
  * ビルドシステムの変更

# 今日お話すること

* ~~Fluentdのプロジェクト体制~~
* ~~開発方針~~
* **最近のリリース情報**
* 最新情報の入手・開発参加方法
* Fluentdの今後

# 最近のリリース情報

昨年のminorアップは3回

* 2021-01-05: v1.12.0
* 2021-05-29: v1.13.0
* 2021-08-30: v1.14.0

{::note}https://github.com/fluent/fluentd/blob/master/CHANGELOG.md{:/note}

# Fluentd v1.12

* in_tail: `follow_inode`の追加
  * {::note}glob指定でもローテーションをいい感じに{:/note}
* in_tail: Linux capability
  * {::note}https://www.clear-code.com/blog/2020/11/27.html{:/note}
* `fluent-ctl`コマンドの追加
  * {::note}シグナルが無いWindowsでもコマンド送信{:/note}

# Fluentd v1.13

* in_tail: スロットリング機能
  * {::note}大きなファイルは少しずつ読み込み{:/note}
  * {::note}大きなファイル読み込み中もすぐに終了可に{:/note}
* ログローテーション設定
  * {::note}`<system>`でも設定可能に{:/note}
  * {::note}主にWindowsサービス用{:/note}

# Fluentd v1.14

* メトリクスの強化
  * {::note}`enable_input_metrics`, `enable_size_metrics`{:/note}
* in_syslog: `send_keepalive_packet`
* 大きなレコードの取り扱い改善
  * {::note}in_tail: `max_line_size`{:/note}
  * {::note}BufferChunkOverflowErrorバグ修正{:/note}

# Fluentd v1.14.2

* CVE-2021-41186
  * parser_apache2のReDoS脆弱性
  * GitHub Security Labからの報告
  * {::note}https://github.com/fluent/fluentd/security/advisories/GHSA-hwhf-64mh-r662{:/note}

# Fluentd 最新リリース

* 2022-02-09: v1.14.5 
  * in_http: x-ndjsonサポート
  * Ruby 3.1での不具合修正
  * out_forward
    * TLS handshakeでの`connect_timeout`

# td-agent リリース情報

* td-agent 3系はEOL
  * {::note}https://www.fluentd.org/blog/schedule-for-td-agent-3-eol{:/note}
* 最新はtd-agent 4.3.0
  * {::note}https://www.fluentd.org/blog/td-agent-v4.3.0-has-been-released{:/note}
  * fluentd 1.14.3

# 今日お話すること

* ~~Fluentdのプロジェクト体制~~
* ~~開発方針~~
* ~~最近のリリース情報~~
* **最新情報の入手・開発参加方法**
* Fluentdの今後

# 最新情報の入手方法

* fluentd.orgブログ記事
  {::note}https://www.fluentd.org/blog/{:/note}
* Fluentd on GitHub
  {::note}https://github.com/fluent/fluentd/blob/master/CHANGELOG.md{:/note}
  {::note}https://github.com/fluent/fluentd/issues{:/note}
* td-agent on GitHub
  {::note}https://github.com/fluent/fluent-package-builder/blob/master/CHANGELOG.md{:/note}
  {::note}https://github.com/fluent/fluent-package-builder/issues{:/note}

# ディスカッションの場所

* GitHub Discussion（NEW!）
  * {::note}https://github.com/fluent/fluentd/discussions{:/note}
* Slack
  * {::note}https://launchpass.com/fluent-all{:/note}
* 以下は廃止予定（NEW!）
  * {::note}https://groups.google.com/g/fluentd{:/note}
  * {::note}https://discuss.fluentd.org/{:/note}

# 日本向けの課題

* 日本語での情報発信
  * {::note}日本のユーザーに情報が届いているのか？{:/note}
* 日本語でバグ報告や議論
  * {::note}日本からの開発参加が少ない印象{:/note}
  * {::note}我々は日本のユーザーの要求を聞けているのか？{:/note}

# 今日お話すること

* ~~Fluentdのプロジェクト体制~~
* ~~開発方針~~
* ~~最近のリリース情報~~
* ~~最新情報の入手・開発参加方法~~
* **Fluentdの今後**

# Fluentdの今後

* リリース頻度の更なる定式化？
  * teenyアップに機能追加は入れない
  * 定期的なminorアップ
* LTS版の提供？

{::note}ジャストアイデアなので決定事項ではありません{:/note}

# Fluentd開発予定

* 当面はissue対応が中心
  * {::note}安心して使えることが第一{:/note}
  * {::note}数あるプラグイン・関連repoのメンテも必要{:/note}
* やりたいこと
  * {::note}https://github.com/fluent/fluentd/wiki/Roadmap-and-Development-tasks#v2{:/note}
* アイデア・プルリク歓迎！

# td-agentの今後

* Ruby 3.1対応
  * Windows版での不具合修正が必要
* 名前の変更?
  * 「td-agent」-> 「fluentd」
* LTS版の提供？

# まとめ

* Fluentd開発体制変更
* メンテナンス継続
* 定期的にリリース
* 開発参加歓迎
* td-agent名前変わるかも

# ご清聴ありがとうございました！
